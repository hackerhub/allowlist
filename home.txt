# HackerHub default allowlist
#
# Project home page:
# https://bitbucket.org/hackerhub/allowlist/
# Author: AlfieJ04

127.0.0.1  localhost
::1  localhost

# [Home]
127.0.0.1 hackerhub.uk

# [Hotspot Shield]
127.0.0.1 control.kochava.com

# [Switch Roms]
127.0.0.1  theonlygames.com

# [Infinite Group]
127.0.0.1  infinitegaming.uk
127.0.0.1  infiniteholding.uk
127.0.0.1  infinitesecurity.uk
127.0.0.1  infinite-trading.uk

# [Avanza]
127.0.0.1  avanza.se

# [Google Ads]
127.0.0.1 ads.google.com
127.0.0.1 ad.doubleclick.net

# [Sentry.io]
127.0.0.1 sentry.io

# [Asus]
127.0.0.1 asus.com

# [1337]
127.0.0.1 1337x.to

# [RARBG]
127.0.0.1 rarbg.to

# [Piratebay]
127.0.0.1 thepiratebay.org

# [Seedrs]
127.0.0.1 customeriomail.com

# [Payslips]
127.0.0.1 yourpayslips.com